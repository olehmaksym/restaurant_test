export const encodeQueryData = (data = {}) => {
  const q = Object.entries(data).map(([key, value]) => ((value && typeof value === 'object')
    ? Object.entries(value).map(([k, v]) => (Array.isArray(v)
      ? v.map((w, i) => `${key}[${k}][${i}]=${w}`).join('&')
      : `${key}[${k}]=${v}`)).join('&')
    : `${key}=${value}`)) || [];
  return q.join('&').replace('&&', '&');
}

export const url = 'https://www.gonogosupport.com/gonogoApi1909Secure/index.php/api_dev/pullExternalMenu'

export const config = {
  client: 'Active Culture',
  storeNums: [1],
  token: 'plHZDxMrf1NzXeElFLdOstRMBkpEdH8b'
}

export const uniqueCategoryArray = (arr) => arr.reduce((accumulator, currentObject) => {
  // Check if the categoryName of the current object already exists in the accumulator array
  const alreadyExists = accumulator.some((category) => category === currentObject.categoryName);

  if (!alreadyExists) {
    // If the categoryName doesn't exist, add the current object to the accumulator array
    accumulator.push(currentObject.categoryName);
  }

  return accumulator;
}, []);
