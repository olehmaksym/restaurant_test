import { useEffect, useState } from 'react'
import { encodeQueryData, url, config, uniqueCategoryArray } from './helpers'

import './App.css'
import { Navigation } from "./components/navigation"
import { ItemsLayout } from "./components/itemsLayout"

function App() {
  const [menuData, setMenuData] = useState(null)
  const [categories, setCategories] = useState(null)
  const [activeCategory, setActiveCategory] = useState(null)

  useEffect(() => {
    fetch(`${url}?${encodeQueryData(config)}`)
      .then((response) => response.json())
      .then(({ desc }) => {
        const cat = uniqueCategoryArray(desc)
        setCategories(cat)
        setActiveCategory(cat[0])
        setMenuData(desc)
      })
      .catch((error) => console.error("Error:", error))
  }, [])

  return (
    <div className="App">
      <header>
        <h1>San Clemente Store Menu</h1>
      </header>
      {menuData
        ? (
          <main>
            <Navigation
              active={activeCategory}
              categories={categories}
              onClick={setActiveCategory}
            />
            <ItemsLayout
              items={menuData.filter(item => item.categoryName === activeCategory)}
            />
          </main>
        )
      : <div>Loading..</div>
      }
    </div>
  );
}

export default App;
