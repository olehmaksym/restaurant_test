import React from 'react'

export const Navigation = ({ categories, active, onClick }) => {
  return (
    <nav className="pure-menu pure-menu-horizontal">
      <ul className="pure-menu-list">
        {categories.map(c => (
          <li className="pure-menu-item" key={c} onClick={() => onClick(c)}>
            <button className={`mx-5 pure-button ${c === active ? 'pure-button-primary pure-button-active' : ''}`}>
              {c}
            </button>
          </li>
        ))}
      </ul>
    </nav>
  )
}
