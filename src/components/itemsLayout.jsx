import React from 'react'

export const ItemsLayout = ({ items }) => {
  return (
    <div>
      {items.map(i => (
        <div key={i.itemNum} className="itemWrapper mb-10">
          <img src={`https://placehold.co/300x200?text=${i.name}`} width="300" height="200" alt={i.name} />
          <div className="itemContent">
            <h2 className="mb-10">{i.name}</h2>
            <p className="mb-10 price">${i.itemPrice}</p>
            <p>{i.description}</p>
          </div>
        </div>
      ))}
      <div>
        {items[0]?.categoryDescription}
      </div>
    </div>
  )
}
